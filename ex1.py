#!/usr/bin/python3.8
import pytest

# content of test_sample.py
def func(x):
    return x + 1

def test_answer():
    assert func(4) == 5

print(f'func returned ', func(4))
print("fix1 fix1")