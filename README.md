this folder contains a simple CI/CD project with the following aims:

- a simple python file
- a simple pytest
- use git
- put in bitbucket
- build/run from jenkins
- create a docker image
- use tox
- package
